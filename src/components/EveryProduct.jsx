import React from 'react'

const EveryProduct = ({ singleProduct, index, handleAddToCart }) => {
    return (
        <div key={index} className="m-4 shadow-lg" style={{ width: "15rem" }}>
            <div
                className="mask text-light d-flex justify-content-center flex-column text-end"
                style={{ width: "50%", marginLeft: "40%" }}
            >
                {singleProduct.isFreeShipping ? <p type="button" className="btn btn-dark btn-sm">Free shipping</p> : <p type="button" className="btn btn btn-sm"><br /></p>}

            </div>
            <img className="card-img-top" src={`${singleProduct.id + 1}.jpg`} alt="Card image cap" />
            <div className="card-body ">
                <p className="card-title d-flex flex-wrap justify-content-center">{singleProduct.title}</p>
                <div className='d-flex flex-wrap justify-content-center'>
                    <hr className='border border-2 border-warning w-25 ' />
                </div>
                <strong className='d-flex flex-wrap justify-content-center font-weight-bold'>${singleProduct.price}</strong>
                <p className='d-flex flex-wrap justify-content-center text-secondary '>{singleProduct.installments > 0 ? " or " + singleProduct.installments + " * $ " + (singleProduct.price / singleProduct.installments).toFixed(2) : "No installments"}</p>
                <button className="btn btn-dark container-fluid"
                    data-bs-open="offcanvas"
                    data-bs-target="#offcanvasRight"
                    aria-controls="offcanvasRight"
                    onClick={() => handleAddToCart(singleProduct)}>
                    Add to cart
                </button>
            </div>
        </div>

    )
}

export default EveryProduct
