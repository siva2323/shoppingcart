import React, { useState } from 'react'
import { useContext } from 'react';
import { Context } from './DisplayAllItems';
const DisplayProductsOnCart = ({ handleDecrement, handleIncrement }) => {

    const productsOnCart = useContext(Context);
    return (
        <div >
            <div className="offcanvas-body">
                {productsOnCart.length === 0 && <p className='text-light d-flex flex-wrap justify-content-center'>Add some products in the cart :) </p>}
                {productsOnCart.length > 0 &&
                    (productsOnCart.map((everyProductOnCart, index) => {
                        return (
                            <div key={index} className="card mb-3 text-light bg-dark" style={{ maxWidth: 540 ,height:""}}>
                                <div className="row g-0">
                                    <div className="col-md-4 mt-2">
                                        <img src={`0${everyProductOnCart.id + 1}.jpg`} className="img-fluid rounded-start" style={{ width: "100px", height: "110px" }} alt="..." />
                                    </div>
                                    <div className="col-md-8  d-flex justify-content-end">
                                        <div className="card-body ">
                                            <h6 className="card-title">{everyProductOnCart.title}</h6>
                                            <p className="card-text ">
                                                {"Quantity: " + everyProductOnCart.quantity}
                                            </p>
                                        </div>
                                        <div className="card-text mt-4 text-warning">
                                            {"$" + (everyProductOnCart.price)}
                                            <div className='d-flex justify-content-end'>
                                                <button className="btn btn-dark" onClick={() => handleDecrement(everyProductOnCart.id)} >-</button>
                                                <button className="btn btn-dark" onClick={() => handleIncrement(everyProductOnCart.id)}>+</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    }))
                }

            </div>
        </div>
    )
}

export default DisplayProductsOnCart