import React, { useEffect, useState } from 'react'
import EveryProduct from './EveryProduct'
import SizeBox from './SizeBox'
import { createContext } from 'react'
export const Context = createContext();

import productDataJson from "./data.json"
import CartSidePanel from './CartSidePanel'

const productData = productDataJson.products
const sizes = [...new Set(productData.flatMap(everyItemObject => everyItemObject.availableSizes))]

const DisplayAllItems = () => {
  const [clickedSizeArray, setClickedSizeArray] = useState([])
  const [productsOnCart, setProductsOnCart] = useState([])
  const [filteredData, setFilteredData] = useState([])
  const [isShowing, setIsShowing] = useState(false)
  let display = isShowing ? "show" : " "
  const contextValue = productsOnCart;

  const handleClickSize = (event) => {
    let newSize;
    if (clickedSizeArray.includes(event)) {
      newSize = clickedSizeArray.filter(item => item !== event)
    } else {
      newSize = [...clickedSizeArray, event]
    }
    setClickedSizeArray(newSize)
  }

  useEffect(() => {
    const modifiedProductData = productData.filter(everyProduct => {
      if (clickedSizeArray.length !== 0) {
        return clickedSizeArray.some(everySize => {
          return everyProduct.availableSizes.includes(everySize)
        });
      }
      return productData
    })
    setFilteredData(modifiedProductData)
  }, [clickedSizeArray])

  const handleDecrement = (productId) => {
    productData.map(everyProduct => {
      if (everyProduct.quantity === 1) {
        return productData;
      }
      else if (everyProduct.id === productId) {
        everyProduct.quantity = everyProduct.quantity - 1;
        setProductsOnCart([...productsOnCart])
      }
    })
  }

  const handleIncrement = (productId) => {
    productData.map(everyProduct => {
      if (everyProduct.id === productId) {
        everyProduct.quantity = everyProduct.quantity + 1;
        setProductsOnCart([...productsOnCart])
      }
    })
  }

  const handleAddToCart = (addedProduct) => {
    setIsShowing(true)
    if (productsOnCart.includes(addedProduct)) {
      addedProduct.quantity = addedProduct.quantity + 1;
      setProductsOnCart([...productsOnCart])
    } else {
      addedProduct.quantity = 1;
      setProductsOnCart([...productsOnCart, addedProduct])
    }
  }
  const handleClose = () => {
    setIsShowing(false)
  }

  return (
    <>
      <Context.Provider value={contextValue}>
        <CartSidePanel handleDecrement={handleDecrement} handleIncrement={handleIncrement} display={display} handleClose={handleClose} />
      </Context.Provider>
      <div className='mx-5 w-100  d-flex flex-row bd-highlight mt-3 ' >
        <SizeBox handleClickSize={handleClickSize} sizes={sizes} clickedSizeArray={clickedSizeArray} />
        <div className=' container'>
          <p className='mx-4'>{filteredData.length || productData.length} Product(s) found</p>
          <div className='w-100 d-flex flex-wrap ' >
            {filteredData.length > 0 ? filteredData.map((singleProduct, index) => {
              return (
                <EveryProduct singleProduct={singleProduct} index={index} handleAddToCart={handleAddToCart} />
              )
            }) : (productData.length > 0) && productData.map((singleProduct, index) => {
              return (
                <EveryProduct singleProduct={singleProduct} index={index} handleAddToCart={handleAddToCart} />
              )
            })}
          </div>
        </div>
      </div>
    </>
  )
}

export default DisplayAllItems

