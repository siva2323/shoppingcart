import React from 'react'

const SizeBox = ({ handleClickSize, sizes, clickedSizeArray }) => {


    return (
        <div className='' style={{ width: "20%" }} >
            <h5>Sizes:</h5>
            <div className="container">
                <div className="row">
                    {sizes.length > 0 && sizes.map(everySize => {
                        return (
                            <button className={` m-2 rounded-circle ${clickedSizeArray.includes(everySize) ? "bg-dark text-white" : ""}`} style={{ width: "3rem", height: "3rem", fontSize: "10px", border: "0" }} onClick={() => handleClickSize(everySize)}>{everySize}</button>
                        )
                    })}
                </div>
            </div>
            <div className='mt-5 '>
                <p>Leave a star on Github if this repository was useful :)</p>
            </div>
            <div>
                <button type="button" className="btn btn-outline-dark btn-sm">☆ Star</button>
                <button type="button" className="btn btn-outline-dark btn-sm">2241</button>
            </div>
        </div>
    )
}

export default SizeBox