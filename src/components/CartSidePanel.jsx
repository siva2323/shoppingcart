import React, { useEffect, useState } from 'react'
import DisplayProductsOnCart from './DisplayProductsOnCart'
import { useContext } from 'react';
import { Context } from './DisplayAllItems';


const CartSidePanel = ({ handleDecrement, handleIncrement, handleClose, display }) => {
    const productsOnCart = useContext(Context);
    const [total, setTotal] = useState(0)

    useEffect(() => {
        let calculateTotal = 0;
        productsOnCart.map(singleProduct => {
            calculateTotal += singleProduct.quantity * singleProduct.price
        })
        setTotal(calculateTotal.toFixed(2));
    }, [productsOnCart])


    let totalQuantity = 0;
    let getQuantity = productsOnCart.map(everyProduct => {
        totalQuantity += everyProduct.quantity;
    })

    const alertCheckOut = () => {
        alert("checkout-Subtotal  : $ " + total)
    }
    return (
        <>
            <button
                className="btn btn"
                type="button"
                data-bs-toggle="offcanvas"
                data-bs-target="#offcanvasRight"
                aria-controls="offcanvasRight"
            >
                <img className='btn btn-dark  fixed-top' style={{ marginLeft: "97%" }} src="bag-icon.png" />
                <p type="button" className="btn btn-warning btn-circle rounded-circle fixed-top btn-sm mt-4" style={{ marginLeft: "98%" }}>{totalQuantity}</p>
            </button>
            <div
                className={`offcanvas offcanvas-end bg-dark  ${display}`}
                tabIndex={-1}
                id="offcanvasRight"
                aria-labelledby="offcanvasRightLabel"
            >
                <button
                    type="button"
                    className="btn-close bg-white text-reset mt-5 mx-2"
                    data-bs-dismiss="offcanvas"
                    onClick={() => handleClose()}
                    aria-label="Close"
                    style={{ color: "white" }}
                />
                <div className="offcanvas-header d-flex flex-wrap justify-content-center">
                    <h5 className="offcanvas-title " style={{ color: "white" }} id="offcanvasScrollingLabel">
                        <img className='btn btn-dark ' src="bag-icon.png" />
                        Cart
                    </h5>
                </div>
                <div>

                </div>
                <div className="offcanvas-body">
                    <DisplayProductsOnCart handleDecrement={handleDecrement} handleIncrement={handleIncrement} />

                </div>
                <div className='offcanvas-footer d-flex flex-wrap justify-content-between'>
                    <div className='offcanvas-footer card shadow p-3 mb-5 bg-body-tertiary rounded bg-black text-white text-secondary '>SUB TOTAL</div>
                    <h4 className='text-warning'>{" $ " + total}</h4>
                </div>
                <button className='container-fluid btn btn-dark' onClick={alertCheckOut}>CHECKOUT</button>
            </div>
        </>
    )
}

export default CartSidePanel