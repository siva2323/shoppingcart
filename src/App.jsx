import { useState } from 'react'
import Header from './components/Header'
import DisplayAllItems from './components/DisplayAllItems'
function App() {

  return (
    <div className="App">
      <Header />
      <DisplayAllItems />
    </div>
  )
}

export default App
